﻿namespace SolveTheBox
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvInput = new System.Windows.Forms.DataGridView();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.dgvSolution = new System.Windows.Forms.DataGridView();
            this.grbSolution = new System.Windows.Forms.GroupBox();
            this.flpSolution = new System.Windows.Forms.FlowLayoutPanel();
            this.nudMaxSteps = new System.Windows.Forms.NumericUpDown();
            this.lblMaxSteps = new System.Windows.Forms.Label();
            this.nudCols = new System.Windows.Forms.NumericUpDown();
            this.nudRows = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSize = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSolution)).BeginInit();
            this.grbSolution.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxSteps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCols)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRows)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvInput
            // 
            this.dgvInput.AllowUserToAddRows = false;
            this.dgvInput.AllowUserToDeleteRows = false;
            this.dgvInput.AllowUserToResizeColumns = false;
            this.dgvInput.AllowUserToResizeRows = false;
            this.dgvInput.ColumnHeadersVisible = false;
            this.dgvInput.Enabled = false;
            this.dgvInput.Location = new System.Drawing.Point(12, 49);
            this.dgvInput.Name = "dgvInput";
            this.dgvInput.RowHeadersVisible = false;
            this.dgvInput.Size = new System.Drawing.Size(317, 216);
            this.dgvInput.TabIndex = 0;
            // 
            // txtInput
            // 
            this.txtInput.Location = new System.Drawing.Point(335, 49);
            this.txtInput.Multiline = true;
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(226, 146);
            this.txtInput.TabIndex = 6;
            this.txtInput.TextChanged += new System.EventHandler(this.txtInput_TextChanged);
            // 
            // dgvSolution
            // 
            this.dgvSolution.AllowUserToAddRows = false;
            this.dgvSolution.AllowUserToDeleteRows = false;
            this.dgvSolution.AllowUserToResizeColumns = false;
            this.dgvSolution.AllowUserToResizeRows = false;
            this.dgvSolution.ColumnHeadersVisible = false;
            this.dgvSolution.Enabled = false;
            this.dgvSolution.Location = new System.Drawing.Point(12, 271);
            this.dgvSolution.Name = "dgvSolution";
            this.dgvSolution.RowHeadersVisible = false;
            this.dgvSolution.Size = new System.Drawing.Size(317, 216);
            this.dgvSolution.TabIndex = 2;
            // 
            // grbSolution
            // 
            this.grbSolution.Controls.Add(this.flpSolution);
            this.grbSolution.Location = new System.Drawing.Point(335, 201);
            this.grbSolution.Name = "grbSolution";
            this.grbSolution.Size = new System.Drawing.Size(229, 284);
            this.grbSolution.TabIndex = 3;
            this.grbSolution.TabStop = false;
            this.grbSolution.Text = "Solution";
            // 
            // flpSolution
            // 
            this.flpSolution.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpSolution.Location = new System.Drawing.Point(3, 16);
            this.flpSolution.Name = "flpSolution";
            this.flpSolution.Size = new System.Drawing.Size(223, 265);
            this.flpSolution.TabIndex = 0;
            // 
            // nudMaxSteps
            // 
            this.nudMaxSteps.Location = new System.Drawing.Point(273, 21);
            this.nudMaxSteps.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMaxSteps.Name = "nudMaxSteps";
            this.nudMaxSteps.Size = new System.Drawing.Size(54, 20);
            this.nudMaxSteps.TabIndex = 5;
            this.nudMaxSteps.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMaxSteps.ValueChanged += new System.EventHandler(this.nudMaxSteps_ValueChanged);
            // 
            // lblMaxSteps
            // 
            this.lblMaxSteps.AutoSize = true;
            this.lblMaxSteps.Location = new System.Drawing.Point(210, 23);
            this.lblMaxSteps.Name = "lblMaxSteps";
            this.lblMaxSteps.Size = new System.Drawing.Size(57, 13);
            this.lblMaxSteps.TabIndex = 4;
            this.lblMaxSteps.Text = "&Max Steps";
            // 
            // nudCols
            // 
            this.nudCols.Location = new System.Drawing.Point(56, 23);
            this.nudCols.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCols.Name = "nudCols";
            this.nudCols.Size = new System.Drawing.Size(54, 20);
            this.nudCols.TabIndex = 1;
            this.nudCols.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.nudCols.ValueChanged += new System.EventHandler(this.nudCols_ValueChanged);
            // 
            // nudRows
            // 
            this.nudRows.Location = new System.Drawing.Point(132, 23);
            this.nudRows.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudRows.Name = "nudRows";
            this.nudRows.Size = new System.Drawing.Size(54, 20);
            this.nudRows.TabIndex = 3;
            this.nudRows.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.nudRows.ValueChanged += new System.EventHandler(this.nudRows_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(114, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "x";
            // 
            // lblSize
            // 
            this.lblSize.AutoSize = true;
            this.lblSize.Location = new System.Drawing.Point(23, 23);
            this.lblSize.Name = "lblSize";
            this.lblSize.Size = new System.Drawing.Size(27, 13);
            this.lblSize.TabIndex = 0;
            this.lblSize.Text = "&Size";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 497);
            this.Controls.Add(this.lblSize);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nudRows);
            this.Controls.Add(this.nudCols);
            this.Controls.Add(this.lblMaxSteps);
            this.Controls.Add(this.nudMaxSteps);
            this.Controls.Add(this.grbSolution);
            this.Controls.Add(this.dgvSolution);
            this.Controls.Add(this.txtInput);
            this.Controls.Add(this.dgvInput);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Solve the Box";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSolution)).EndInit();
            this.grbSolution.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxSteps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCols)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRows)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvInput;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.DataGridView dgvSolution;
        private System.Windows.Forms.GroupBox grbSolution;
        private System.Windows.Forms.FlowLayoutPanel flpSolution;
        private System.Windows.Forms.NumericUpDown nudMaxSteps;
        private System.Windows.Forms.Label lblMaxSteps;
        private System.Windows.Forms.NumericUpDown nudCols;
        private System.Windows.Forms.NumericUpDown nudRows;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSize;
    }
}

