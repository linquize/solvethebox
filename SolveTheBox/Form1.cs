﻿using System;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace SolveTheBox
{
    public partial class Form1 : Form
    {
        struct MoveStep
        {
            public int X { get; set; }
            public int Y { get; set; }
            public bool RightOrUp { get; set; }
        }

        int miCols;
        int miRows;
        Color[] maColors = { Color.White, Color.Red, Color.Orange, Color.Yellow, Color.YellowGreen, Color.Blue, Color.Purple };
        int[,] miaInput;
        int miMaxStep;
        DateTime mdtSolve = DateTime.MaxValue;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            miCols = (int)nudCols.Value;
            miRows = (int)nudRows.Value;
            miMaxStep = (int)nudMaxSteps.Value;
            BuildGrid();
            new Thread(Solve).Start();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            txtInput.Focus();
        }

        private void nudCols_ValueChanged(object sender, EventArgs e)
        {
            miCols = (int)nudCols.Value;
            RunOnUIThread(() => flpSolution.Controls.Clear());
            BuildGrid();
        }

        private void nudRows_ValueChanged(object sender, EventArgs e)
        {
            miRows = (int)nudRows.Value;
            RunOnUIThread(() => flpSolution.Controls.Clear());
            BuildGrid();
        }

        private void nudMaxSteps_ValueChanged(object sender, EventArgs e)
        {
            miMaxStep = (int)nudMaxSteps.Value;
            RunOnUIThread(() => flpSolution.Controls.Clear());
            mdtSolve = DateTime.Now.AddSeconds(2);
        }

        private void txtInput_TextChanged(object sender, EventArgs e)
        {
            RunOnUIThread(() => flpSolution.Controls.Clear());
            string[] lsaLines = txtInput.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            if (miRows > lsaLines.Length)
                lsaLines = Enumerable.Repeat("", miRows - lsaLines.Length).Concat(lsaLines).ToArray();
            for (int i = 0; i < miRows; i++)
            {
                if (lsaLines.Length <= i)
                {
                    for (int j = 0; j < miCols; j++)
                    {
                        miaInput[miRows - i - 1, j] = 0;
                    }
                }
                else
                {
                    string lsLine = lsaLines[i];
                    for (int j = 0; j < miCols; j++)
                    {
                        if (lsLine.Length <= j)
                        {
                            miaInput[miRows - i - 1, j] = 0;
                        }
                        else
                        {
                            int liColor = lsLine[j] - '0';
                            if (liColor < 0 || liColor > 9 || liColor >= maColors.Length) liColor = 0;
                            miaInput[miRows - i - 1, j] = liColor;
                        }
                    }
                }
            }

            UpdateGrid(dgvInput, miaInput);
            mdtSolve = DateTime.Now.AddSeconds(2);
        }

        void RunOnUIThread(Action apAction)
        {
            if (this.InvokeRequired)
                this.Invoke(new Action(apAction));
            else
                apAction();
        }

        void BuildGrid()
        {
            dgvInput.Rows.Clear();
            dgvInput.Columns.Clear();
            for (int i = 0; i < miCols; i++)
                dgvInput.Columns[dgvInput.Columns.Add("colInput" + i, (i + 1).ToString())].Width = dgvInput.RowTemplate.Height;
            for (int i = 0; i < miRows + 1; i++)
                dgvInput.Rows.Add();
            dgvInput.Rows[miRows].Visible = false;
            dgvInput[0, 0].Selected = false;
            dgvInput[0, miRows].Selected = true;

            dgvSolution.Rows.Clear();
            dgvSolution.Columns.Clear();
            for (int i = 0; i < miCols; i++)
                dgvSolution.Columns[dgvSolution.Columns.Add("colSolution" + i, (i + 1).ToString())].Width = dgvSolution.RowTemplate.Height;
            for (int i = 0; i < miRows + 1; i++)
                dgvSolution.Rows.Add();
            dgvSolution.Rows[miRows].Visible = false;

            dgvSolution[0, 0].Selected = false;
            dgvSolution[0, miRows].Selected = true;

            miaInput = new int[miRows, miCols];
        }

        void Solve()
        {
            while (!this.IsDisposed)
            {
                Thread.Sleep(1000);
                if (mdtSolve > DateTime.Now) continue;
                mdtSolve = DateTime.MaxValue;
                RunOnUIThread(() => 
                {
                    flpSolution.Controls.Clear();
                    flpSolution.Controls.Add(new Label { Text = "Solving ...", AutoSize = true });
                });

                int[,] liaModel = new int[miRows, miCols];
                Array.Copy(miaInput, liaModel, liaModel.Length);

                int liStart = Environment.TickCount;
                MoveStep[] lvtSteps = new MoveStep[miMaxStep];
                bool lbSolved = TryMoveTile(liaModel, 0, lvtSteps);
                int liTimeSpent = Environment.TickCount - liStart;
                if (lbSolved)
                {
                    RunOnUIThread(() =>
                    {
                        flpSolution.Controls.Clear();
                        flpSolution.Controls.Add(new Label { Text = "Solved in " + liTimeSpent + " ms", AutoSize = true });
                        for (int i = 0; i < miMaxStep; i++)
                        {
                            MoveStep lvtStep = lvtSteps[i];
                            var loRadio = new RadioButton
                            {
                                Text = string.Format("{0}: X = {1}, Y = {2}, Direction = {3}", i + 1, lvtStep.X, lvtStep.Y, lvtStep.RightOrUp ? "Right" : "Up"),
                                AutoSize = true,
                                Tag = lvtSteps.Take(i + 1).ToArray()
                            };
                            loRadio.CheckedChanged += (o, e) =>
                            {
                                var laSteps = ((MoveStep[])((RadioButton)o).Tag);
                                MoveTileBySteps(laSteps, laSteps.Length);
                            };
                            flpSolution.Controls.Add(loRadio);
                        }
                    });
                }
                else
                {
                    RunOnUIThread(() =>
                    {
                        flpSolution.Controls.Clear();
                        flpSolution.Controls.Add(new Label { Text = "No solution, time: " + liTimeSpent + " ms", AutoSize = true });
                    });
                }
            }
        }

        bool TryMoveTile(int[,] aiaModel, int aiStep, MoveStep[] aaSteps)
        {
            if (aiStep >= miMaxStep) return false;

            int[,] liaModel = new int[miRows, miCols];
            for (int h = 0; h < 2; h++)
            {
                for (int i = 0; i < miRows - 1; i++)
                {
                    for (int j = 0; j < miCols - 1; j++)
                    {
                        Array.Copy(aiaModel, liaModel, liaModel.Length);

                        bool lbMoved = false;
                        if (h == 0)
                            lbMoved = SwapTile(liaModel, j, i, j + 1, i);
                        else if (liaModel[i, j + 1] != 0)
                            lbMoved = SwapTile(liaModel, j, i, j, i + 1);

                        if (lbMoved)
                        {
                            if (FallTile(liaModel))
                            {
                                aaSteps[aiStep] = new MoveStep { X = j, Y = i, RightOrUp = h == 0 };
                                return true;
                            }

                            if (TryMoveTile(liaModel, aiStep + 1, aaSteps))
                            {
                                aaSteps[aiStep] = new MoveStep { X = j, Y = i, RightOrUp = h == 0 };
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        void MoveTileBySteps(MoveStep[] aaSteps, int aiLength)
        {
            int[,] liaModel = new int[miRows, miCols];
            Array.Copy(miaInput, liaModel, liaModel.Length);
            for (int i = 0; i < aiLength; i++)
            {
                MoveStep lvtStep = aaSteps[i];
                bool lbMoved = false;
                if (lvtStep.RightOrUp)
                    lbMoved = SwapTile(liaModel, lvtStep.X, lvtStep.Y, lvtStep.X + 1, lvtStep.Y);
                else if (liaModel[lvtStep.Y, lvtStep.X + 1] != 0)
                    lbMoved = SwapTile(liaModel, lvtStep.X, lvtStep.Y, lvtStep.X, lvtStep.Y + 1);

                if (lbMoved)
                    FallTile(liaModel);
            }

            UpdateGrid(dgvSolution, liaModel);
        }

        bool SwapTile(int[,] aiaModel, int x1, int y1, int x2, int y2)
        {
            int liTile1 = aiaModel[y1, x1];
            int liTile2 = aiaModel[y2, x2];
            if (liTile1 == liTile2) return false;
            aiaModel[y1, x1] = liTile2;
            aiaModel[y2, x2] = liTile1;
            return true;
        }

        bool FallTile(int[,] aiaModel)
        {
            for (int j = 0; j < miCols; j++)
            {
                for (int q = 0; q < miRows; q++)
                {
                    for (int p = 0; p < miRows; p++)
                    {
                        if (aiaModel[p, j] == 0)
                        {
                            for (int i = p + 1; i < miRows; i++)
                                aiaModel[i - 1, j] = aiaModel[i, j];
                            aiaModel[miRows - 1, j] = 0;
                        }
                    }
                }
            }

            return CheckMatch(aiaModel);
        }

        bool CheckMatch(int[,] aiaModel)
        {
            bool lbFall = false;
            for (int i = 0; i < miRows; i++)
            {
                int liSame = 1;
                for (int j = 1; j < miCols; j++)
                {
                    int liColor = aiaModel[i, j];
                    if (liColor != 0 && liColor == aiaModel[i, j - 1])
                    {
                        liSame++;
                    }
                    else
                    {
                        if (liSame >= 3)
                        {
                            for (int k = j - 1; k >= j - liSame; k--)
                            {
                                aiaModel[i, k] = 0;
                            }
                            lbFall = true;
                        }
                        liSame = 1;
                    }
                }

                if (liSame >= 3)
                {
                    for (int k = miCols - 1; k >= miCols - liSame; k--)
                    {
                        aiaModel[i, k] = 0;
                    }
                    lbFall = true;
                }
            }

            for (int i = 0; i < miCols; i++)
            {
                int liSame = 1;
                for (int j = 1; j < miRows; j++)
                {
                    int liColor = aiaModel[j, i];
                    if (liColor != 0 && liColor == aiaModel[j - 1, i])
                    {
                        liSame++;
                    }
                    else
                    {
                        if (liSame >= 3)
                        {
                            for (int k = j - 1; k >= j - liSame; k--)
                            {
                                aiaModel[k, i] = 0;
                            }
                            lbFall = true;
                        }
                        liSame = 1;
                    }
                }

                if (liSame >= 3)
                {
                    for (int k = miCols - 1; k >= miCols - liSame; k--)
                    {
                        aiaModel[k, i] = 0;
                    }
                    lbFall = true;
                }
            }

            if (lbFall)
                return FallTile(aiaModel);

            for (int i = 0; i < miRows; i++)
            {
                for (int j = 0; j < miCols; j++)
                    if (aiaModel[i, j] != 0)
                        return false;
            }
            return true;
        }

        void UpdateGrid(DataGridView aoGrid, int[,] aiaModel)
        {
            for (int i = 0; i < miRows; i++)
            {
                for (int j = 0; j < miCols; j++)
                {
                    var loStyle = new DataGridViewCellStyle(aoGrid.DefaultCellStyle);
                    int liColor = aiaModel[miRows - i - 1, j];
                    loStyle.BackColor = maColors[liColor];
                    aoGrid[j, i].Style = loStyle;
                }
            }
        }
    }
}
